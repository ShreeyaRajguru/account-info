import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import TableAcc from './components/Table';
import 'semantic-ui-css/semantic.min.css';
import { Table } from 'semantic-ui-react';


ReactDOM.render(
  <React.StrictMode>
    <App />
    <TableAcc />
  </React.StrictMode>,
  document.getElementById('root')
);

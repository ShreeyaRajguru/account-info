import React from 'react';
import {Header, Icon} from 'semantic-ui-react';

function App() {
  return (
    <div>
    <Header as='h2' icon textAlign='center'>
      <Icon name='users' circular />
      <Header.Content>Account and User Info</Header.Content>
    </Header>
  </div>
  );
}

export default App;

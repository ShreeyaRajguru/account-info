import React, {Component} from 'react'
import axios from 'axios';
import {Redirect} from 'react-router-dom'
import { Container, Button, Table } from 'semantic-ui-react'

export default class TableAcc extends Component <any, any>{
    constructor(props: any) { 
        super(props);
        this.state = {
            data:[],
            userdata:null
        };
    }

    async componentDidMount(){
        const url = "http://localhost:5000";
        const response = await fetch(url);
        const data = await response.json();
        
        this.setState({
            data: data,
        })
    }

     viewUser = async (e:any) => {
         let fetchData = {
            method: 'POST',
            body: JSON.stringify({ id: e.target.value }),
            headers: { 'Content-Type': 'application/json' }
          }
         const url2 = "http://localhost:5000/createdBy"
         const response2 = await fetch(url2, fetchData)
         const data2 = await response2.json();
         console.log(data2);
         this.setState({
            userdata: data2
        })
        alert(JSON.stringify(this.state.userdata, null, 8))
    }

    
    
    render(){
        return(
            <Container>      
                <Table celled>
                    <Table.Header>
                    <Table.Row>
                        <Table.HeaderCell>Account ID</Table.HeaderCell>
                        <Table.HeaderCell>Account Name</Table.HeaderCell>
                        <Table.HeaderCell>Account Desc</Table.HeaderCell>
                        <Table.HeaderCell>User Data</Table.HeaderCell>
                    </Table.Row>
                    </Table.Header>

                    <Table.Body>
                    {this.state.data.map((d:any) => {
                        return(
                        <Table.Row>
                            <Table.Cell>{d.id}</Table.Cell>
                            <Table.Cell>{d.name}</Table.Cell>
                            <Table.Cell>{d.description ? d.description:"Null"}</Table.Cell>
                            <Table.Cell><Button primary value={d.createdUser} onClick={this.viewUser}>View User Info</Button></Table.Cell>
                        </Table.Row>
                        )
                    })}
                    </Table.Body>
                </Table>
            </Container>
        )
    }
}
const express = require('express');
const cors = require('cors');
require('dotenv').config();

const app = express();
const port = process.env.PORT || 5000;

app.use(cors());
app.use(express.json());

const{
    Schemas: {Account},
    Services: {AccountService},
} = require('@smartserv/transformer');

app.get('/',async (req, res) => {
    try{
        let q = {};
        const acc = new Account(q);
        var accounts = await AccountService.find(acc);
        res.json(accounts);
        //console.log(accounts);
    }
    catch(err){
        console.log(err);
    }
});

const{
    Schemas: {UserDetails},
    Services: {UserDetailsService},
} = require('@smartserv/transformer');

app.post('/createdBy',async (req, res) => {
    try{
        let q = new UserDetails({id:Number(req.body.id)});
        var users = await UserDetailsService.find(q);
        res.json(users[0]);
        
    }
    catch(err){
        console.log(err);
    }
});

app.listen(port, () => {
    console.log('Server is running on port:'+port);
});
